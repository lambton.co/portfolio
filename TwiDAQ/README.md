# TwiDAQ for iOS

### Overview

TwiDAQ (https://play.twidaq.com) is a Twitter based trading game. Whilst at Deep Blue Sky I implemented a significant design upgrade and vastly improved the performance and stability of the iOS app.

### Information

**Role:** UI/UX Design & Application Development

**Language(s):** Objective-C

**Role involvement:** 50%

**Status:** Removed from App Store in 2018