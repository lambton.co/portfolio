# DSC - Digital Shopping Channel

### Overview

DSC is a new and innovative shopping experience focussed on delivering product discovery via videos on their YouTube channel. They required a native mobile application to integrate with their YouTube channel and their existing Magento e-commerce platform. This allowed users to quickly and easily purchase products whilst watching the various video episodes uninterrupted.

### Information

**Role:** UI/UX Design & Application Development

**Language(s):** Swift

**Role involvement:** 100%

**Status:** Delivered / Released 2016 (App was subsequently removed from the App Store and the company appears to no longer be trading)