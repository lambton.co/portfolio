# Nationwide Building Society

### Overview

Working with Nationwide’s Experience team, I provided specialist mobile and technical consultancy, delivering interface and experience design across several major service updates being made to customer and colleague facing products.

### Information

**Role:** UI/UX Design & Technical Consultant

**Language(s):** N/A

**Role involvement:** 100% UI / 50% UX

**Status:** Delivered 2017