# Castrol

### Overview

The Castrol Speciality Products app was developed to allow sales employees to easily browse, filter and view information for the companies product range in several languages.

### Information

**Role:** UI Design & Application Development

**Language(s):** Objective-C

**Role involvement:** 50% Design / 100% Development

**Status:** Delivered 2013