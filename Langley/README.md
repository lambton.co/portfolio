# Langley Roofing Systems

### Overview

Langley roofing required a complete CRM system to replace their paper-based system - this was developed by Deep Blue Sky. I joined DBS to undertake the design and development of an iPad application that could be used remotely, on-site, to further reduce load and paperwork on staff and to tightly integrate with the CRM system.

### Information

**Role:** UI Design / Application Development

**Language(s):** Objective-C

**Role involvement:** 100%

**Status:** Delivered 2014