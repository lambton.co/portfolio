# Bristol University

### Overview

One of my more unusual products. I worked with the Animal Welfare department at Bristol University to plan, design and deliver a cloud-based system with supporting native mobile applications to streamline the tracking and management of livestock for farmers. Legacy paper-based processes were time-consuming and inefficient, the new digital platform aimed to simplify the entire process, making it quick and simple for all users.

### Information

**Role:** UI/UX Design / System Design & Development / Application Development

**Language(s):** HTML, SCSS, Javascript, PHP, Objective-C

**Role involvement:** 100%

**Status:** Delivered / Released 2015