# Legal & General

### Overview

Legal & General marketing team required an installation for a planned event. The concept was a fairly simple penalty-shootout game, tying in with the 2012 World Cup.

### Information

**Role:** UI Design & Application Development

**Language(s):** Objective-C

**Role involvement:** 100%

**Status:** Delivered 2012