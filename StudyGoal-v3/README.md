# Study Goal v3.0

### Overview

Study goal is part of the Jisc Learning Analytics Architecture, and is currently available to pilot institutions.

Study Goals borrows ideas from fitness apps, allowing students to see their learning activity, set targets, record their own activity amongst other things. It also has a social side, allow students to share their activity and goals with their peers.

### Information

**Role:** UI/UX Design & Application Development

**Language(s):** Swift

**Role involvement:** 100%

**Status:** Due for release September 2019