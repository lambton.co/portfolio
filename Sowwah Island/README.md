# Sowwah Island Installation

### Overview

Sowwah Island in Dubai began construction in 2010, one of the first buidings constructed was the hotel in the central plaza. The developers marketing department required an installation to feature in the hotel that allowed visitors to explore the island and development progress. The application was eventually deployed on an 80 inch television in the hotel lobby, as well as on iPads elsewhere.

### Information

**Role:** UI Design & Application Development

**Language(s):** Unity3D / C#

**Role involvement:** 50% Design / 100% Development

**Status:** Delivered 2012